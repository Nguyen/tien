# tien.nguyen.re

A simple identity page for Tien Nguyen made with Hugo and LoveIt theme. This is v2:
![v2](v2.png)

v1 of the page was made using Jekyll and Identity theme by html5up:
![v1](v1.png)